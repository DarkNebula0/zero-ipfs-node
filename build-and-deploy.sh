#!/bin/sh

echo ### Build Docker Image ###
docker build -t lactore/zero-ipfs-node:latest .

echo ### Upload image ###

docker login

docker push lactore/zero-ipfs-node:latest

docker logout

echo ### End ###