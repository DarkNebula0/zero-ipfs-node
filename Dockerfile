FROM ipfs/go-ipfs:latest

ENV SWARM_PEER="/ip4/167.86.85.161/tcp/4001/ipfs/12D3KooWRjRoJSdjfvPY7KrhcignwSVuP69oBUu5NSxRX7dJu6iW"
ENV IPFS_SWARM_KEY="/key/swarm/psk/1.0.0/\n/base16/\nb305c3b7e9388eaa310c9bf9860ad505457a60da3e00ce1d98b40f4512793c84"
ENV LIBP2P_FORCE_PNET="1"

COPY init.sh /usr/local/bin/start_ipfs
RUN chown ipfs:users /usr/local/bin/start_ipfs && chmod +x /usr/local/bin/start_ipfs